# Vi and Vim common commands
## Installation and other stuff
To install the full Vim package:
```
    sudo apt install vim
```

vimtutor comes with the Vim package and is great for learning Vim.
```
    vimtutor
```
## Command mode stuff
Exit and save the file:
```
    ZZ or :wq
```
Quit without saving:
```
    ZQ or :q!
```
***If your Vim sessions is frozen, you may have hit `Ctrl-s` by accident. Hit `Ctrl-q` to recover.***

### Copy/Cut/Paste commands
```
  yy ## copy a line
  yw ## copy a word
  y3 ## copys 3 lines

  dd ## delete/cut line
  dw ## delete/cut word
  d4 ## delete 4 words after the cursor

  p ## paste

  u ## undo the last change
  Ctrl-r or :redo ## redo the last change
```
### Repeating a command
Use `.` to repeat the last command.
### Repeating a character
Typing a number followed by 'i', a character, and then ESC will repeat that character *number* times. For example, `5iU<ESC>` will repeat the 'U' character 5 times.
### Indenting lines
Use `>` to shift to the right, `<` to shift to the left in the following commands.

These commands will indent spaces according to how 'shiftwidth' is set.

`>>` will indent the line the cursor is on.

To indent five lines: `5>>`  

To indent the entire paragraph, no matter where your cursor is in tha paragraph, use `>ap`. To indent everything at the cursor until the end of paragraph, use `>}`

To indent curly braces block, put your cursor on one of the curly braces and use: `>%`. To shift what's inside the block, put cursor inside block and use `>iB`  

If you're copying blocks of text around and need to align the indent of block to its new location, use `]p` instead of just `p`

### Commenting code use visual mode
1. Put your cursor at the beginning of the first line you want to comment.
1. Press `Ctrl-v`
1. Press `j` until you've highlighted the first character of every line you want to comment.
1. Press `Shift-i`
1. Type in the characters you use to comment your code, e.g. `//`.
1. Press the escape key.

## Using ':'
### Commenting code using substitution
The following will insert `// ` at line 29 to line 50: 
```
    :29,50s/^/\/\/ /
```

This will remove `// ` from those lines: `:29,50s/^\/\/ //`

### Substitution
`:s/search/replace` is the basic command. This will search within the line only for the first occurrence of *search* and replace it with *replace*. To search all occurrences within the line, add `/g` to the end:
```
  :s/search/replace/g
```
To search the entire file, add a percent sign to the front:
```
  :%s/search/replace/g
```
To confirm each replacement, add `/c`:
```
  :%s/search/replace/gc
```
To search and replace in a range of lines:
```
  :14,30 s/search/replace/g 
```
### Vim Options
Turn on and off escape chars:
```
    :set list
    :set nolist
```
This will turn on and off numbers:
```
    :set nu
    :set nonu
```
Turn on and off relative numbers:
```
    :set relativenumber
    :set norelativenumber
```
Turn on shell syntax:
```
    :set syn=sh
    :set syntax=sh
```
Use help to look up something, then quit:
```
    :help rtp
    :q
```
Tabpages. Use `gt` or `gT` to switch between tabpages:
```
    :tabe /tmp/anotherfile
```
Split the window to edit another file. Use `Ctrl-w` `w` to switch between files, `Ctrl-w` `j` or `k`, etc, will go up or down like regular vim, and `Ctrl-w` `Ctrl-j` (or k, etc) will move the pans around.:
```
    :sp /tmp/anotherfile
```
### Buffers

Open multiple files with Vim.  

With a file open in Vim, `:edit` or `:e` will open a second file if it exists or create a new file if it doesn't.  This command puts the file in the active buffer, e.g. the file you see in Vim:
```
  :edit ~/.vimrc
  :e ../etc/apt/.conf
```
To open or create a new file in the background, use `:badd`:
```
  :badd ~/.vimrc
```
Delete buffer:
```
  :bdelete <filename>
  :bd <filename>
```
List buffers:
```
  :ls
```
Move between buffers:
```
  :bp ## Move to previous buffer (file)
  :bn ## Move to next buffer
  :b3 ## Move to the file in the 3rd buffer
  :bf ## Move to the first buffer
  :bl ## Move to the last buffer
```
Working with buffers in Vim windows:
```
  :split <filename> ## Open <filename> in a horizontal window 
  :vsplit <filename> ## Open <filename> in a vertical window 
  :ball ## Open all buffers in horizontal windows
  :vertical ball ## Open all buffers in vertical windows
  :hide ## Hide the current window/buffer
  :only # Hide all other buffers/windows, but keep the current active buffer (curent window) open.
```


## !!/Magic Wands
Using !! followed by a command/script/etc will send the line the cursor is on to that command and replace that line with its output (this is magicwands):
```
    !!<command>
```
## Shell Integration with Magic Wands FTW
*stolen from <https://gitlab.com/rwx.gg/README/-/blob/master/vi/README.md>*

*Magic wands* are a mnemonic device for using the exclamation point to send the current line, section, or page to any command that can be run from the shell and replace those lines with the output of that command. In fact, once you master the magic wands you can use the same keystrokes to do find and replace and other tasks that use `ex` mode just by backspacing out the exclamation point.

### Line Wand

The line wand is by far the most frequent wand you will use. It is the fastest and most common way to extend `vi`. Just spam bang twice to send the current line to the shell command or just replace the current line with the output of a utility command like `date`, `cal` or *any* text that comes out of *any* command.

#### Send Line to Bash: `!!bash`

```bash
for i in {1..20}; do echo Item $i; done
```

#### Send Line to Calculator: `!!bc`
*This requires the `bc` program to be installed, which is by default on most Linux systems.*  

```bc
232432 * 2342342 / 234
```

#### Send Line to Python3: `!!python3`

```python
[print(f"Item {x}") for x in range (1,21)]
```

### Section Wand

Sending a section is a great way to run part of a larger script (say for configuration) without having to cut it out and put it into another file. If you use this easy way, don't forget to undo it later to replace it with the original code (instead of the output of the lines of code).

#### Send Section to Bash: `!}bash`

```bash
for i in {1..20}; do
  echo Item $i
done
```

### Line Number Wand: `!:<lineafter>`

Line number wand is just a larger version of the [section wand] but allows blank lines to be included. It takes more keystrokes, however. Start with a bang and follow it up with a colon and the line *after* the last line that you want to include.

For the following example imagine the first line is line 10 of the file and you want to send it to `pandoc` for rending as HTML. You would position your cursor on the first `#` (on line 10) and type `!:22<enter>` (because there are 11 lines to send). Then type `pandoc<enter>` after the `!` as the command to receive the lines and replace them.
