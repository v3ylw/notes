# Linux Beginner Boost Day 7
## Hosting providers
Besides AWS, Azure, and Google, there is Digital Ocean and Linode. Also Rackspace. Linode might make you pay upfront.

Digital Ocean has a simple user interface. Popular with learners and developers.

## Digital Ocean setup
* create an account. cheapest will cost $5/month, but if you delete the droplet immediately, should only cost you 50c/1buck a month or so.
* create a droplet
* add sshkey from pub file
* from local computer, ssh to public IP provided by droplet: 
```
    ssh root@IPaddress
```
Copy your bashrc file (if you want) to DO. If you don't include the colon at the end of IPaddress, it will create a file on the remote system called "root@IPaddress":
```
    scp ~/.bashrc root@IPaddress:
```
