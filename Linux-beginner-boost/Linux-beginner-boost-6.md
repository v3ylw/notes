# Linux Beginner Boost Day 6
## Certifications

Only one Rob recommends:
<https://www.lpi.org/our-certifications/linux-essentials-overview>

Linux Essentials objectives:
<https://wiki.lpi.org/wiki/Main_Page>

LPIC-2 the cert to say you are a Linux Professional.  Minimum $90k/yr.

## WSL2
Ubuntu, actual kernel, not emulated

## Licenses
<https://choosealicense.com>  
Ran by gitlab.com

Rob's favorite permissive license is Apache 2. Lawyers like it.  

<https://creativecommons.org> is for written content, not software.

Linux released under GPL2.  
GNU released under GPL3.

GPL2 basically: if you make changes, I want the changes. GPL3 basically: GPL2, plus you have to allow user to change software on device.

## Distros
<https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg>  
PopOS: desktop Linux that takes care of UEFI and drivers. Nice for beginners.

Linux Mint is one Rob uses for a long time. Ultra stable.

Rob recommends using an Ubuntu-based system as your first distro.  

RedHat still big in the enterprise. Use Fedora for desktop if you want a RedHat distro or work in a RedHat environment.

These days, probably biggest difference between distros is package management. RPM for RedHat, Deb for Debian, Arch is yay/pacman.

Arch has very uptodate user repository.  
Have to know your stuff with Arch.  
Vanilla Arch just a basic distro.  
Packages are not necessarily vetted. Be careful.

Not a distro, but a book: <http://www.linuxfromscratch.org/lfs/>. A huge undertaking, but also a big cred.

Gentoo is a distro where you have to compile everything.
