# Linux Beginner Boost Day 17
## More Bash Scripting
Exporting a variable makes it available to subprocesses/subshells.

Command substitution - single parens
```sh
  echo "today is $(date)"
```
To do math functions -  use nested parens
```sh
  echo "Result of 2 times 2: $((2*2))"
```
### Here documents
Kind of a form of redirection.  
echo works with strings.  Here documents work with cat.  
doesn't seem to matter what \_EOF\_ is.
```sh
TITLE="System Report"
cat << _EOF_
  <html>
    <head>
      <title>Web page title</title>
    </head>
    <body>
    <h1>Here's the report</h1>
    <p>$TITLE</p>
   </body>
  </html>
_EOF_
```
### echo with double quotes to pass it as is
```sh
  declare answer=not

  declare rant="
  John
  is
  $answer
  here.
  "

  echo "rant"
```
### Reading Interative Input
```sh
  declare name="$1"
```
Declaring "$2" would indicate indicate it's asking for the second
argument.  "$0" is always the "first" argurment, which is always the
thing that is type when running the script/app (what's actually typed,
not the location of the script/executabe).

When you read numbers from the command line, they always come in as
strings, not integers, so you have to convert it.

### Functions
Commands, in a very real way, is a way of running a function. Every
single command is the same thing as if you are calling a function, a
subroutine, or a procedure, etc.  

A function is just a mini script.

If you dot source a bash file with a function in it, you make that
function available on the command line.

Rob differentiates beween functions and procedures. He defines functions
as pure functions, that have no side effects and return something. He
defined bash functions as procedures, where are meant to have side
effects, only return error codes/etc.

To run a function, just call it in the script.  Don't need parens.

### If statement

## Basic JSON
Stands for JavaScript Object Notation.

Everything in JSON is a collection. Two types of collections, objects and
arrays.

<https://api.github.com>  
<https://pokeapi.co>

Marshalling is putting data structures into a flat file (JSON file).
Unmarshalling is the reverse.
## YAML 
Ansible, Docker uses YAML. Go can read YAML.
