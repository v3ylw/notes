# Linux Beginner Boost Day 10
Appimage: the Belena Etcher usb drive creator is one. unzip it, chmod if
necessary, and run it.

PWA, progressive web apps, is software you can click and it will download
and run it. Saves you from an extra copy of Electron. PWAs use whatever
is on the system, Chrome, Firefox, whatever, instead of the copy of
Chromium built in every Electron. Every Electron app (Discord, Slack) has full version
of Chromium, full version of Node, and a websocket communication system.
This uses a lot of RAM.

## Software Package Managers
<https://www.linode.com/docs/tools-reference/linux-package-management/>  
This includes commonly-used apt commands.

Arch:  
* AUR/ Arch User Repository. No vetting. Anyone can put anything in it.
Packages can be outdated.
* pacman: vetted by arch devs.

Ubuntu:
* besides the default package repository (Canonical?) you can use PPAs
  to add other locations to get software.
* Devs can create deb packages and put them in Launchpad to get them out,
  apparently.

Debian Package Manager
* apt
* dpkg

RedHat Package Manager
* yum

SUSE
* yast

Windows
* chocolately

## Understand Professional Linux Occupations
<https://rwx.gg/occupations/>

All employment is based on trust.  No one is going to hire you unless you
can prove you can do what you say.  

Identify the role, industry and hopefully top three companies you would like to
work for. You need to know what you like to know what role you want.

As a beginner programmer, you will mostly be working on someone else's
code and fixing/updating/maintaining.  
Most companies want a fast programmer instead of a good quality
programmer.

Set up your LinkedIn to show your projects, point to Github? Gitlab?

When the economy is good, big companies put money into RD, product
development, marketing, advertising.  Good time to be a developer.  When
the economy is bad, big companies are interested in maintaining what they
have, so it's a good time to be in operations, not development.  They
actually fire developers during downturns. **If you keep your coding
skills and your operations skills sharp, you can survive and thrive.**
