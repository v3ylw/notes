# Linux Beginner Boost Day 8
## Virtualization
Qemu and KVM are hypervisors for Linux.

HyperV is a type 1 HV and Vbox is type 2.   If you are using HyperV you need nested virtualization to run Vbox.

Above means you cannot run hyperv and vbox at the same time without special config.

Sounds like it may be impossible to run Vbox and Docker at the same time.

You *can* run VBox on WSL.

When setting up a new virtual box, under networking:
* NAT - nothing outside the host will see those guest VMs.
* Bridged - will be accessible by systems outside of host.

## Containerization
<https://rwx.gg/contain>

Containers don't provide state. You have to explicitly to send data out to a file or db. No internal storage management in a container.

Kubernetes manages containers across multiple systems. It decides where a container should be based on the container's requirements. Kubernetes is like a process scheduler for containers.

<https://hub.docker.com>

<https://rwx.gg/docker> for instructions on using Docker. 

Docker command line reference: <https://docs.docker.com/engine/reference/commandline/docker/>

## Docker commands
Install docker:
```
    sudo apt install docker.io
```
Put your user into the docker group if you don't want to run sudo everytime:
```
    sudo usermod -aG sudo $USER
```
Test docker installation by running Hello World container.  This will download the image from hub.docker.com and run it:
```
    docker run hello-world
```
Run a docker container (ubuntu), give it a name ("test"), and connect to an interactive terminal. The ubuntu container is very minimal:
```
    docker run --name test -it ubuntu
```
Show which containers are running:
```
    docker ps
```
Show containers still on system, even ones not running:
```
    docker ps -a
```
Use the remove command to get it out of docker ps -a:
```
    docker rm container-ID
```
To stop a docker container that is running:
```
    docker kill container-ID
```
Or use this? I think use this one instead:
```
    docker stop container-ID
```
Remove all containers:
```
    docker container prune
```
Run docker daemonized (so it stays running), then connect to it with a default command:
```
    docker run --name test -it -d ubuntu bash
    docker exec -it test bash
```
Same this as above, but mount a local directory:
```
    docker run --name test -it -d -v /home/asdfasdf:/home/asdfasdf ubuntu bash
    docker exec -it test bash
```
You will probably want to immediately update your container if it's a debian distro (run this after connecting via exec or the -it switch):
```
    apt update
```
Create a new docker image (let's you save stuff you added):
```
    docker commit containerID-or-name new-name
    docker commit test test1
```
See all downloaded images.  docker container prune and docker system prune will not get rid of these:
```
    docker images
```

