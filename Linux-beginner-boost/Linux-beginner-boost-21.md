# Linux Beginner Boost Day 21
## Markdown
Universal standard for basic knowledge transfer.

## Pandoc
Pandoc is univeral document converter
<https://pandoc.org>

Pandoc uses the \$ symbol, so it must be escaped with a backslash.

This is an example of having a style class associated with a section. This lets Pandoc apply the style using whatever you are converting to. This is Pandoc-specific, basic markdown doesn't use this:
 ```md
  Some text{class name}
 ```

Need to install pandoc, Rob recommends using the latest.
<https://gitlab.com/rwxrob/dotfiles/common/installs/install-pandoc>

Convert a file:
```sh
pandoc markdownfilename 
```

Pandoc by default converts to html

Using a template to add basic HTML stuff:
```sh
pandoc --quiet -s --template=template.html markdownfilename
```
Send to a file:
```sh
pandoc --quiet -s --template=template.html -o index.html
markdownfilename
```
