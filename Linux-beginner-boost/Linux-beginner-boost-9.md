# Linux Beginner Boost Day 9
[Bad advice](https://rwx.gg/stupid/)

## Flash OS images to SD cards and USB drives 
Don't use 'dd' command to create a USB drive (if you are a beginner). Doesn't use checksums to check if copy was correct; will overwrite target without warning if you choose wrong target, wrong byte size means your drive may not keep up.

Rufus gets boot sectors wrong on images, don't use it.

Use [Balena Etcher](https://balena.io/etcher).
* Download the zip file
* 'unzip' the file
* chmod +x if necessary
* ./run the appimage

## Understanding the Boot Process
BIOS =>  
Boot loader on boot partition =>  
OS on OS partition

Disk partitions are how you divide your disk.  

The MBR is on the first 512(?) bytes of the boot partition. It tells the computer where to look on the rest of the boot partition to keep booting.

Windows and Macs come with recovery partitions. In the case of Macs, if it doesn't have a recovery partition, the BIOS can download an installation (which means Mac BIOS has a network stack, PXE Boot).

GRUB is a boot loader.

### UEFI
Originally designed to protect from someone putting a USB into a computer and booting into it to bypass security measures. (Safe boot)

May need to turn of UEFI safe boot to get Linux to work.

### Live Booting
* Booting of a USB
* Can be persistent

Rubber Ducky: USB drive that you can boot into and capture data from a
computer. Dangerous to use, use on a test computer.

## Disk Encryption
PopOS and Mint have disk encryption. Others may have it too.

## Making your Linux system safe
* Evaluate your security needs (need disk encryption?)
* Pick a secure distro
* 'sudo apt update' and 'sudo apt upgrade' immediately
* Get a WiFi dongle (Alfa)--it can be put into permiscuous/monitor mode.
  Some WiFi drivers (Broadcom is notorious) don't work with Linux.
* Consider Ethernet over WiFi
* Create a user with sudo
* Check for open listeners
* Consider backup strategies
