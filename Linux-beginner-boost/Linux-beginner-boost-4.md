# Linux Beginner Boost Day 4
Rob's config files:  
https://gitlab.com/rwxrob/config

Use magic wands instead of Vim plugins

when you first log in to a shell, it runs a number of system scripts, then runs bashrc script in home dir.

the bashrc (usually) calls a bash alias file, where you can add customizations.  You can change bashrc on your own systems but may not be able to do so on other systems where it is locked down.

bashprofile only runs on logins, not when, say, set up to run as a task on the system via cron

use 'type' to see aliases
```
    type ll
```
'. bashconfig' in the directory where bashconfig exists (dotfiles in my case) sourced(?) the file and made it live.

if you put a backslash in front of an alias, it will run the real program.

use 'which' to see where the program is located.  'which vi' should output '/usr/bin/vi'

neat trick to see the symbolic link of the which command:
```
    ls -l $(which vi)
```
is the same as:
```
    ls -l /usr/bin/vi
```
Create symbolic link from bashconfig in present working directory (dotfiles) to .bash_aliases in home directory:
```
    ln -s $PWD/bashconfig $HOME/.bash_aliases
```
The place where your computer looks for scripts, executables, is $PATH.  To see $PATH, echo it.  It will show every directory (separated by colons) where exes and scripts will run, in the order that it looks for it:
```
    echo $PATH
```
This will replace current running shell with new one:
```
    exec bash
```
## tmux
terminal multiplexer  
a way to split your screen into multiple parts

## xclip
let's you pipe the output of a terminal command into the mouse buffer so you can paste somewhere else, like vim:
```
    cal | xclip
```
'|' actually takes the output of the previous command and sends it into the input of the next command. '>' sends to a file.

