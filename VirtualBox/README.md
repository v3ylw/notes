## Install and Enable Guest Additions on new systems
(taken from [The Odin Project](https://www.theodinproject.com/courses/web-development-101/lessons/prerequisites?ref=lnav#step-2-install-virtualbox-and-set-up-xubuntu))

Your regular operating system (Windows in this case) is called the Host, and all other operating systems that run as VMs are called Guests. To make working in your Guest OS easier, you need to install Guest Additions. It adds useful functionality to the Guest OS, such as full-screen guest mode.

While your VM is running, do the following steps:

1. Install all available updates. If there are no available updates, move on to Step 3.
1. If the Software Updater is stuck waiting for an unattended upgrade to finish, reboot the VM and start again from Step 1.
1. Open a terminal with ctrl + alt + t or opening the Whisker Menu and typing in Terminal (the shortcut is obviously faster).
1. Copy and paste this into the terminal: `sudo apt install linux-headers-$(uname -r) build-essential dkms`. Enter your password when it asks you to.
1. If you get the following errors: Unable to locate package build-essential and Unable to locate package dkms, paste in the following: `sudo apt-get install build-essential` and enter your password. Otherwise, move on to Step 6.
1. Type Y when it asks you to and let it finish installing. Close the terminal when it is finished.
1. Click Devices on the VM toolbar -> Insert Guest additions CD image in the menu bar.
1. Wait for the CD image to mount, it will show the CD on the desktop as solid, not transparent, and a window will show on the top right of the VM screen saying it was successfully mounted.
1. Double-click on the CD icon on the VM desktop.
1. In the new window that opens, right click on the white-space or any file/folder, and click Open Terminal Here.
1. In the newly opened terminal window, paste `sudo ./VBoxLinuxAdditions.run` and hit enter. You will know it is finished when it asks you to close the window.
1. Once it finishes, close the terminal and the CD folder.
1. Right-click CD on the VM desktop and click Eject Volume. It will not eject if the CD folder is open.
1. Reboot your VM (which you can do by typing `reboot` and hitting enter in a terminal).
1. You can now maximize the VM window, use the shared clipboard, and create additional displays, among many other useful features. These options are available on the VM toolbar under View and Device.

NOTE:

If upon trying to start the VM you only get a black screen, close and “power off” the VM, click “Settings -> Display” and make sure “Enable 3D Acceleration” is UNCHECKED, and Video memory is set to AT LEAST 128mb.

If you receive an error when trying to mount the Guest Additions CD image (“Unable to insert the virtual optical disk”), please reboot your host (Windows/OSX) operating system. Afterwards, ensure that there is no image file mounted in both Virtual Box as well as in the file system of the VM.

## Updating already existing Guest Additions
2020-06-04: after upgrading to VirtualBox 6.1.8 from 6.0.xx on my computer (which already had Guest Addtions installed), the VBox Guest Additions 6.1.8 CD was already mounted on my VM.  All I had to do was start at step 9 in the "Install and Enable Guest Additions on new systems" section. On my Ubuntu, I guess I hadn't run the step 4 commands yet, because when I tried step 9, it warned me that build-essential wasn't installed.  I installed it and then ran step 9 again, it uninstalled the previous attempt first.
