# JSON
JavaScript Object Notation

A structured data language.

Sweet spot is anything that has to be read or exchanged with the
Internet.

You will encounter JSON when dealing with anybody's APIs.
```sh
  curl https://api.github.com | jq .
```
*`.` means select the entire content of the json file that is coming in and print it out nicely.*

To bring down Github's list of APIs to a file:
```sh
  curl https://api.github.com | jq . > /tmp/github.json
```
To read a JSON file:
```sh
  jq . file.json
```
A JSON file is an object with key:value pairs.

All string types must have double quotes.  
You can never have an actual line break inside a value.  
Can't do comments.

JSON is fast, it's meant to be parsed.

## Types
Numbers, strings, floating points, 1.2e10, negative numbers, empty strings, true and false, null

Everything in JSON is some combination of these types.

You can make an array of different types.  You can nest arrays.
```json
  [1,34,[-13,45e10],"string",true]
```
JSON can refer to objects as maps. Map, hash, dictionary, object are all names used for what JavaScript calls objects.
## jq
```sh
  jq .name types.json
  jq -r .name types.json
  jq .age types.json
  jq '.interests[0]' types.json
```
The first line will show all the values whose keys are named "name". The second line with `-r` will give the raw output of that same information. The fourth line will give the first item in the array of the key(s) called "interests"*need the single quote around it because bash sees square brackets as something else*.

