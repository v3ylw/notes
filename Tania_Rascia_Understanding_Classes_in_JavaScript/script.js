class Hero {
  constructor(name, level) {
    this.name = name
    this.level = level
    this.sayNo = function(value) {
      if (value.toLowerCase()==='no') {
        return `${value} was said.`
      } else {
        return `${value} is not no.`
      }
    }
  }

  greet() {
    return `${this.name} says hello.`
  }
}

const hero1 = new Hero("Bill",4);
console.log("A new object called 'hero1' based on the 'Hero' class:");
console.log(hero1);

class Mage extends Hero {
  constructor(name, level, spell) {
    //chain constructor with super
    super(name, level)

    // add a new property
    this.spell = spell
  }
}

const hero2 = new Mage('Jill',2,"Magic Missile");
console.log("\nA new object called 'hero2', created on the 'Mage' class that was extended from the 'Hero' class:");
console.log(hero2);
