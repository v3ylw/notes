# Tania Rascia - Understanding Classes in JavaScript 

<https://www.taniarascia.com/understanding-classes-in-javascript/>

Until ECMAScript 2015 (often referred to as ES6) introduced classes, [constructor functions](../Mosh_OOP_Programming_in_JavaScript/) were used to mimic an object-oriented design pattern in JavaScript.

The introduction of classes provided a newer, cleaner syntax without offering new functionality.

A JavaScript class is a type of function. Running 'typeof' against a class will return "function" (so will Object.getPrototypeOf(class)).

## Initializing a class definition
Like a constructor function, the name of the class is capitalized.

A constructor method is used to assign initial(?) properties and methods to a new object:
```js
  class Hero {
    constructor(name, level) {
      this.name = name
      this.level = level
    }

    greet() {
      return `${this.name} says hello.`
  }

  const hero1 = new Hero("Bill",4);
  // creates a new object called hero1 with key:value pairs name:"Bill" and level:4
```
In the code above, the greet function is available to hero1 through its prototype, which is Object with a constructor named class Hero:
```js
  Object.getPrototypeOf(hero1)
  // output will be Object; expanded, it will show the greet function
  console.log(hero1)
  // output will be Object; 'name' and 'level' will be properties directly available, while the greet function will be under the prototype
```
## Extending a Class
Classes can be extended into new object blueprints based off the parent.  This prevents repetition of code for objects that are simlar but need some additional or more specific features.

With ES6 classes, the 'super' keyword is used to access the parent functions.  'extends' refers to the parent class:
```js
  class Mage extends Hero {
    constructor(name, level, spell) {
      // chain constructor with super
      super(name, level)

      // add a new property
      this.spell = spell
    }
  }

  const hero2 = new Mage('Jill',2, "Magic Missile")
```
In the code above, the prototype for 'hero2' is class 'Mage', and the prototype for 'Mage' is class 'Hero'. The greet function is still under the the 'Hero' prototype and still available to 'hero2'.
