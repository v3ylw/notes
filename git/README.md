# Git commands

## Git global setup
```sh
git config --global user.name "user"
git config --global user.email "user@protonmail.com"
```

## 'git add'
Use this command to add files to the git staging area.

To use the interactive version, use `git add -p`. This will allow you to choose which changes you want to add. This only works for files that are tracked.

## 'git blame'
`git blame <filename>` will show you who made changes to <filename> along with the commit hash it was done with.

## 'git branch' 
List existing branches:
```sh
  git branch -v
```
To create a new branch at the current HEAD, type the first command:
```sh
  git branch branch1
  git checkout branch1
```
This will create a new branch called *branch1* at the commit where HEAD points to, and then the second command will switch you to that branch. A shorter version of this is to type `git checkout -b <branchname>`.

## 'git cat-file'
Use this command with the `-p` option to drill down into refs, trees and blobs. For instance, use the **git log** command to find the hash (40 character sequence) of a commit, then use cat-file to drill down:
```sh
  git cat-file -p 6c33b27c9c8ee35f49fdc596058dc4c9a240dde2
```
The above should display the tree hash, the parent hash, along with author, date and commit message. You can continue drilling down by doing a cat-file on the tree hash:
```sh
  git cat-file -p bc2443a368d9116ae510c1e9790dd7005a488790
```
This will show the blob hashes along with associated file names. Finally, doing a cat-file -p on a blob will show you the contents of the file, like the **cat** command.

## 'git checkout'
`git checkout <filename>` will change your file back to its state of the commit where HEAD points to.

Use `git checkout <branchname>` to change to that branch.

You also use this command to move to a specific commit in your commit history. *This changes the contents of your git director to that state!* This also moves where the **HEAD** pointer is. Use in conjunction with the commit hash:
```sh
  git checkout b23392fc025206129d2383a1f0f1e08d4cc4b97d
```
*You can actually just type the first six characters of the hash, you don't have to type the whole thing.* 

If you were to run **git log -all** after this command, you should see the HEAD is not at the same place as **master**. To go back to the latest commit, type:
```sh
  git checkout master
```

## 'git clone'
Create a new local repository:
```sh
git clone git@gitlab.com:<username>/asfda.git
cd asdfa
touch README.md
git add README.md
git commit -m "add README"
# first push requires '-u'
git push -u origin master 
```
To set up a new linux box with my settings: in my home folder, create the following folders: repos/gitlab.com/asfda (note, asdfa is not the actual username).  Then use git clone to create a local copy of the dotfiles.git repo. Using and authenticating with SSH for the git file will allow your to update from that local copy. Using an HTTP link for the git file will only be a read-only copy.

## 'git diff'
Shows you what changed since the last commit, for tracked files (i.e., new files not 'git add'ed will not show up in 'git diff'). You can pass file names as well as commit hashes to be more specific.

## 'git init'
Make an existing folder and Git repo, add a new
remote repo and push the folder to it:
```sh
cd existing_folder
git init
git remote add origin git@gitlab.com:<username>/<project name>.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
To stage a folder to become a remote repository, move to the folder and run:
```sh
  git init --bare
```

## 'git log'
`git log --all` will show the history of your commits. **HEAD** indicates the commit you are currently "pointing to". In the output below, it shows that HEAD is currently pointing to the commit with hash 6c33b2... It also shows that is the latest commit on the master branch. The top entry is the latest commit. Each commit will give the hash reference of the commit (40 char sequence), the author and date, along with the commit message. Example output:
```sh
commit 6c33b27c9c8ee35f49fdc596058dc4c9a240dde2 (HEAD -> master)
Author: rpllpr <email+github@gmail.com>
Date:   Thu Jul 2 19:06:53 2020 -0500

    second commit

commit b23392fc025206129d2383a1f0f1e08d4cc4b97d
Author: rpllpr <email+github@gmail.com>
Date:   Thu Jul 2 18:59:49 2020 -0500

    Add hello.txt
```
**git log** by itself shows a flattened version of your commit history. Another way to show history is to pass some arguments to the command. This will show additional merge/branching info and lines along the left side to indicate the branches, to better illustrate the history of the repo:
```sh
  git log --all --graph --decorate
```
`git log -all --graph --decorate --oneline` shows a more concise version of the above.

## 'git merge'
Use this command to merge branches: `git merge <branchname>`. This will merge <branchname> to the branch you are currently on. Probably a good idea to be on the master branch when using this command.

To abort a failed merge attempt, use `git merge abort`. I believe `git merge continue` will let you continue to a merge that failed for some reason.

## 'git remote' 
View remote repos:
```sh
git remote -vv
```

Add a remote repo called 'upstream' to an existing local repo *(to make a new local repo, use git init or git clone)*:
```sh
git remote add upstream https://gitlab.com/location/of/repo.git
```

Change the URL for a remote named 'origin':
```sh
git remote set-url origin git@gitlab.com:<username>/eightball.git

```

Push an existing local Git repository:
```sh
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:<username>/<project name>.git
git push -u origin --all
git push -u origin --tags
```

## 'git reset'
To remove a file from the staging area, using `git reset <filename>` usually works but specify HEAD to avoid confusion with a *tree-ish* argument:
```sh
  git reset HEAD <filename>
```
To revert to the last accepted commit.  Warning: this will overwrite and remove files that were added since that commit:
```sh
  git fetch
  git reset --hard HEAD
```
## 'git status'
Shows the current status of a folder compared to the last commited change.
