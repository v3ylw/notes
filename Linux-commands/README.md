# Basic Linux terminal commands
Update and upgrade is the first thing you should do on a newly installed systems:
```sh
  sudo apt update
  sudo apt upgrade
```
## apt
`apt list` lists all available software packages. Trying to pipe to grep or more may dislay a message saying APT doesn't have a stable CLI. You can instead use `dpkg --list | grep nginx` to filter on specific packages. 
`apt list -a sudo` tells you if the sudo package is installed or not.  
`apt list --installed` lists all installed packages on Ubuntu.  
`apt update` is used to resynchronize the package index files from their sources. The lists of available packages are fetched from location(s) specified in `/etc/apt/sources.list`  
`apt upgrade` is used to install the new version of all packages currently installed on the system from the sources enumerated in `/etc/apt/source.list`. Current packages installed that have new versions available are retrieved and upgraded.

### apt configuration and files
/etc/apt contains the APT configuration folders and files.  
`apt-config` is the APT Configuration Query program. `apt-config dump` shows the configuration.

/etc/apt/source.list - locations to fetch pacakages from.  
/etc/apt/source.list.d/ - additional source fragments.  
/etc/apt/apt.conf - APT configuration file.  
/etc/apt/apt.conf.d/ - APT configuration file fragments.  
/etc/apt/preferences.d/ - directory with version preference files. This is where you specify 'pinning'; i.e., a preference to get certain packages from a separate source or from a different version of a distribution.
/var/cache/apt/archives/ - storage area for retrieved package files.  
/var/cache/apt/archives/partial/ - storage are for package files in transit. 
/var/lib/apt/lists/ - storage are for state information for each package resource specified in 'source.list'.  

## cd
### 'cd' with double tab
'cd' with double tap will show you possible directories. If you have more than one directory that starts with the letter D (Documents, Desktop, Downloads), and you type:
```sh
    cd D
```
and then double tab immediately after typing 'D', you will get the three directories listed.

### 'cd -'
will put you into the last directory.  You can keep typing that to go back and forth between directories.

## Ctrl commands  
*Note: some of these commands for moving around and editing don't work in Ubuntu,
perhaps because I've `set -o vi` in bash_aliases.*

Ctrl-c sends a sigint (interrupt) that stops the foreground running process.

Ctrl-s suspends your terminal, which means you can't do anything within your terminal.  Ctrl-q will bring it back. 

Ctrl-d sends EOF to the terminal.  This will close the bash shell.
Similar to running the 'exit' command.

Ctrl-z puts running process into background.  Run 'fg' or 'fg
%bg\<process_number\>', e.g `fg %1` to bring it back.

Ctrl-a goes to the beginning of the line. Ctrl-e goes to the end.  
Alt-b goes back one word. Ctrl-b goes back one character.  
Alt-f goes forward one word.  Ctrl-f goes forward one character.  

Ctrl-xx: Move between the beginning of the line and the current position of the cursor. This allows you to press Ctrl+XX to return to the start of the line, change something, and then press Ctrl+XX to go back to your original cursor position. To use this shortcut, hold the Ctrl key and tap the X key twice.

Ctrl-d deletes the character under the cursor.  
Alt-d deletes all the characters after the cursor on the current line.  
Ctrl-h or Backspace deletes the character before the cursor.

Alt-t swaps the current word with the previous word.  
Ctrl-t swaps the last two chars before the cursor with each other. Good
for fixing typos.

Ctrl-\_ undoes the last key press. Can be repeated.

Ctrl-w cuts the word before the cursor, puts in clipboard.  
Ctrl-k cuts the part of the line after the cursor, puts in cb.  
Ctrl-u cuts the part of the line before the cursor, puts in cb.  
Ctrl-y pastes the last thing you cut from the cb. Y stands for yank.

Alt-u capitalizes every char from the cursor to the end of the current
word.  
Alt-l uncapitalizes every char from the char to the end of the current
word.  
Alt-c capitalizes the char under the cursor.  Moves you to the end of the
current word.

## curl
get the tmux config file from Rob's repo and redirect it into a file called tmux.conf:
```sh
    curl https://gitlab.com/rwxrob/dotfiles/-/raw/master/common/tmux/tmux.conf > tmux.conf
```
Download the papermc minecraft zip file. '-L' tells curl to follow redirection. '-o' lets you specify the downloaded file name, instead of using the name of the file you are downloading. The URL is copied directly from the papermc.io website's download page:
```sh
    curl -L -o paper.jar https://papermc.io/api/v1/paper/1.15.2/282/download
```
Use the '-O' command to download the file and keep the name the same as it is on the remote server. The following will download the DEB file to the current directory:
```sh
    curl -O https://proton.me/download/bridge/protonmail-bridge_3.2.0-1_amd64.deb
```

## df
Disk Free command. `-h` shows size info in Human-readable format. `-a` shows All/everything including virtual file systems.
```sh
  df -h
  df -ah
```

## dpgk
Used to install, remove, and provide inforation about .deb packages. A low level tool. 'apt', a higher level tool, is more commonly used as it deals with package dependencies/versioning/etc.

## du
Disk Usage command. To view the total space used by a directory, run:
```sh
  dh -sh <directoryname>
```

### echo variations
```sh
'echo -n' turns off line return  
'echo -e' escape things that are escapable
```

## file
Determine types of files. `file /bin/bash` tells you what type of file bash is. Use **which** command to find out the location of the file:
```sh
user@Ubuntu11:~$ which bash
/bin/bash
user@Ubuntu11:~$ file /bin/bash
/bin/bash: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=12f73d7a8e226c663034529c8dd20efec22dde54, stripped
```

## find
Find a string in file. This finds the string *stdlib.h*:
```sh
  find . -type f -name '*.c' -print0 | xargs -0 grep 'stdlib.h'
```
Without xargs, grep will try to find the string ‘stdlib.h’ in the file names of C files - the output of the find command.  With xargs, the output of the previous command is passed as an argument to the next command. 

xargs can be thought of as turning its stdin into arguments to the given commands

Another way of passing the output of find to other programs is to use `-exec`, see below.

### Options
`-exec`
Takes an external program and passes the output of find to it. For example:
```sh
  find . -type f -exec grep -q 'uname' {} \; -exec ls -la {} \;
```
This will find all files of type 'file', then pass that to grep to look for the string 'uname' in it; if it succeeds, it will then pass that file to ls -la. The result is that it will print out the ls -la results of all files with the text 'uname' in them. `grep -q` does not produce output, just an exit status--the '-q' is needed for this to work. The `{}` tells find to replace it with the pathname currently being processed. The `\;` tells find to know where to end, but escape the backslash so bash doesn't use it instead.

`-name` and `-iname`
`find . -name File.txt`: `-name` is case-sensitive. `find . -iname File.txt`: `-iname` is case-insensitive. Will find FILE.TXT, file.txt, etc.

`-size`
To find a file over a certain size:
```sh
  find / -type f -size +1000M
```
Use can use the following size descriptions:
* c - bytes
* k - kilobytes
* M - Megabytes
* G - Gigabytes
* b - 512-byte blocks

`-type`
Find files of a certain type. Choices are:
* f - regular file
* d - directory
* l - symbolic link
* c - character devices
* b - block devices

`!` in front of another argument will look for the falsy of that argument. For example, to find files that are not executable:
```sh
  find . ! -executable
```

## gzip
Compress a file (this will remove the original file):
```sh
  gzip <filename>
```
Uncompress a gzipped file:
```sh
  gunzip file
```

## history
Show previous CLI entries:
```sh
  history
```
Putting a space before a command in bash will not add that command to history.

## hostname
`hostname` displays the host name of the system.

Show all IP addresses used by the system:
```sh
  hostname --all-ip-addresses
```

## ip
Newer command that replaces `ifconfig`. Type `ip` by itself to see arguments to pass to drill down into more info. To see the IP addresses assigned to interfaces, use:
```sh
  ip addr show
```

## last
'last' shows the last five logins to the system.

## less
Pipe to 'less' to give you options to scroll the output.
* \/ to search
* 'n' for next on search
* 'p' for previous on search
* 'q' to quit

## ls
To print just the file/folder name on separate lines. That's a one, not lowercase L:
```js
  ls -1
```

## lsb_release
One way to show Linux distro version info: `lsb_release -a`. See [uname](#uname) for another way to get info about the system. 
system.

## man
Man pages common elements:
* Items between \[brackets\] are optional.
* If you see \{a\|b\} you must choose.
* And ... means you may have more of the preceding item.

Man pages sections:
* 1 is for end-user commands
* 8 is for administrator (root) commands
* 5 is for configuration files

Many man pages have examples near the end. Otherwise, they'll have related items near the end.  
Use `/sometext` to search for 'sometext'.  
'q' to quit the man page.

If you don't get something back from man when you know you should, run `mandb`:
```sh
  mandb
```
Search for man pages related to 'user':
```sh
  man -k user
```
Search for admin sections of man pages that were found searching for 'user':
```sh
  man -k user | grep 8
```
Filter on '8' and 'create' of the man pages that were found searching for 'user':
```sh
  man -k user | grep 8 | grep create
```

## mount
Mount a filesystem or check for existing mounted filesystems.

To check for existing mounted filesystems, just run `mount` by itself.

To mount a filesystem, run `mount <absolute path to disk> <mountpoint>`:
```sh
  mount /dev/sda2 /mnt
```
To automatically mount a filesystem on boot, look at your /etc/fstab file.

## netstat
Show network connections, routing tables, interface stats, etc.

Show TCP, UDP, listening, PID/Program Names, numeric instead of names of ports or interfaces:
```sh
  sudo netstat -tulpn
```

## Path 
Adding a path to the \$PATH environment
```sh
  export PATH=~/bin:"$PATH"
```

## ps
View running processes on the system.

To view the init process (PID 1), which is the process that has no parent process (PPID is 0):
```sh
  ps -f 1
```
The init process is started by the kernel, in what's called the kernel space. The init process is the first process in the user space. The init process will continue to load other processes in user space.

View processes with 'init' in the name or path:
```sh
  ps -aux | grep init
```
See also **top**.

## service
On older systems/systems that don't use systemd init system, this command provides service configuration
```sh
  service <servicename> status
  service udev status
  service udev start
```
See **systemctl** for systemd's version of the command.

## set
`set -x` will turn on shell debugging. `set +x` turns it off.

## 'source' and sourcing
The dot (.) command is a synonym for the 'source' command, a shell builtin that reads the specified file of shell commands and treats it like input from the keyboard:
```sh
  . .bashrc
```

## SSH
System-wide client configuration files: <https://en.wikibooks.org/wiki/OpenSSH/Client_Configuration_Files>
### known_hosts
The known_hosts file (~/.ssh/known_hosts for users, /etc/ssh/ssh_known_hosts for system-wide) is for verifying the identity of other systems.

known_hosts will be hashed if `HashKnownHosts yes` is set in an SSH config file (/etc/ssh/ssh_config or ~/.ssh/config).
### ssh
After adding your public key to gitlab.com, test to see if you can successfully authenticate:
```sh
  ssh -T git@gitlab.com
```
### ssh-keygen
Tool for creating, viewing, etc., SSH keys. <https://www.ssh.com/ssh/keygen/>

Create a SSH key with the key type 'ed25519' and the file name 'test-ed25519':
```sh
  ssh-keygen -f test-ed25519 -t ed25519
```
View the fingerprint of a key. The '-f' option specifies the file to read. Use the '-v' option to see verbose output with fancy ascii art:
```sh
  ssh-keygen -l -f test-ed25519.pub
  ssh-keygen -lv -f test-ed25519.pub
```
Search a hashed known_hosts file for a host name:
```sh
  ssh-keygen -F gitlab.com
```

## su
Stands for Switch User.

On some systems, you can open a root shell with `su -`. Minus tells it to open a shell. Since a username wasn't provided, it's using root. This doesn't work on Ubuntu; use `sudo -i` instead.
## sudo
On Ubuntu systems (and others), root doesn't have a password, so `su -` doesn't work. Do this instead:
```sh
  sudo -i
```
To rerun a command but with sudo, use `sudo !!`. This will rerun the previous command. Good for when you forget you need elevated priveleges.

## systemctl
systemd's version of the service command. Used for service management tasks. See **service** for non-systemd version of this command:
```sh
  systemctl status <servicename>
  systemctl start udev
  systemctl stop udev.service
```

## tar
Create a tar file:
```sh
  tar -cf archive.tar file1.txt file2.c file3.mpeg
```
Unarchive a tar file:
```sh
  tar -xf archive.tar
```
View the contents of a tar file. Works on a .tar.gz file too:
```sh
  tar -tvf archive.tar
```

## top
A way to view processes that's more in the style of Task Manager in Windows. Press `q` to quit. See also the **ps** command for another way to view processes.

`Shift-m` to sort by memory usage. `Shift-p` to sort by processor usage.

## touch
touch actually updates the timestamp on a file, but it will create the file if it doesn't exist.

## type
For each argument, indicate how it would be interpreted if used as a
command name. `type ll` shows me it's an alias; `type bash` shows me the
location of bash.

## uname
'uname' displays system information including kernel info. See [lsb_release](#lsb_release) for another way to display Linux distro information in Xubuntu.

To see all system info:
```sh
  uname -a
```

## uptime
See the uptime of the server

## wget
Download the homepage of google into a file named google.html. That's a capital 'o', not zero:
```sh
  wget https://google.com -O google.html
```

## which
Locate a command. `which bash` will show the location of the bash
command.

## while
Nifty command to repeatedly execute a command. Handy for testing scripts.
Ctrl-c to break out:
```sh
  while true; do clear; jq . /tmp/types.json; sleep 1; done
```

## whoami
Display the current user name
