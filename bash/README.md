# Bash
Use `#` to make comments in scripts.
## Variables
Bash variables are weakly typed, you do not have to define data type when declared.

You do not have to use a special character when declaring a variable, but you must use `$` to read from it:
```sh
  myvar="Title"
  echo $myvar
```
Make sure there are no spaces in the assignment or bash will think you are trying to run a program called 'myvar'.

No need for '+' or concat operator when combining variables:
```sh
  myvar1=50
  myvar2="The price of this ticket is $"
  echo $myvar2$myvar1
```

You can print the value of a variable without any quotation but if you use quotations then you have to use double quotations.

Use double parens to make arithmetic work:
```sh
  n=100
  echo $n+20 # outputs '100+20'
  ((n=n+20))
  echo $n # outputs '120'
```
## Arrays
Elements of an array are separated by a space in bash:
```sh
  myarray=(Cardinals Blues Chiefs Royals)
```
The following contructs are available for arrays:
```sh
  ${myarray[*]} # All of the items in the array
  ${!myarray[*]} # All of the indexes in the array
  ${#myarray[*]} # Number of items in the array
  ${#myarray[0]} # Length of item zero
```
These constructs can be used in the following ways:
```sh
  echo "Array size: ${#myarray[*]}"

  echo "Print array items:"
  for item in ${myarray[*]}
  do
    printf "%s\n" $item
  done

  echo "Print array indexes:"
  for item in ${!myarray[*]}
  do
    printf "%d\n" $item
  done

  echo "Print index and items:"
  for index in ${!myarray[*]}
  do
    printf "%4d: %s\n" $index ${myarray[index]}
  done

  echo "Print the length of an item:"
  echo {#myarray[0]}
```

