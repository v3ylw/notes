# TMUX
A descendent of screen.

Tmux allows you to disconnect and still have the session going. You can nest sessions. Multiple people can connect to a screen.

Pane: a division of a window. Window or Screen: will show a new window.

## Commands
Tmux uses ctrl-b as a prefix by default, screen uses ctrl-a. My tmux.conf unbinds ctrl-b and sets tmux to use ctrl-a as a prefix.

The prefix tells the system that a tmux command is next. `Ctrl-a` then `char`.

`ctrl-a c` will create a new window.

`ctrl-a n` will scroll forward through windows.
`ctrl-a p` will scroll back through windows.

`ctrl-a d` will detach a tmux session. `tmux a` from bash will attach to the last session.

`ctrl-a w` will give you a list of screens to choose from

`ctrl-a z` will zoom (fill the terminal) the current pane. Hit it again to change it back.

### put this in a tmux pane to continually run a script to see its output while you are working on it
'while true; do ./greet; sleep 1; clear; done'

### declare variables in bash
