# Object-oriented Programming in JavaScript: Made Super Simple | Mosh
<https://www.youtube.com/watch?v=PFmuCDHHpwk>

**Note: see [Understanding Classes in
JavaScript](../Tania_Rascia_Understanding_Classes_in_JavaScript/) to learn information about JavaScript classes, which are alternatives to factory functions and constructors**

OOP is a paradigm, not a tool or framework.  
C#, Java, Ruby, Python, JavaScript (controversial), etc. support OOP.  
Angular is framework that has OOP in mind.

## 4 Key Pillars of OOP
* Encapsulation
* Abstraction
* Inheritance
* Polymorphism

### Encapsulation
Grouping related properties and methods into an object. This reduces complexity and increases reusability.

An object's methods often act on its own properties. As a result, you don't have to have parameters for those methods (functions) in OOP. With procedural programming, you often have to pass a lot of arguments.

### Abstraction
Hiding properties and methods from outside the object. This makes the object's interface simpler, and reduces the impact of change. Reduces complexity and isolates impact of changes.

### Inheritance
Inheriting properties and methods from a parent class or more generic object, and inherting from that class/object. Eliminates redundant code.

### Polymorphism
"Many forms." 

A technique that allows you to get rid of long if/elses and case statements.  

## Object literals
```js
  const circle = {
    radius: 1,
    location: {
      x: 1,
      y: 1
    } 
    draw: function() {
      console.log('draw');
    }
  }
```
Objects are made of key:value pairs.

"radius", "location" and "draw" are members of the "circle" object. Members can be properties or methods. "radius" is the key, "1" is the value. "draw" is the key, its following function is the value.

If an object has one or more methods, we say that object has behavior. 

Using object literals to create objects becomes problematic when creating multiple objects with the same members, because you have to fix the members for each object if they are changed. Instead, use **constructor functions** or **factory functions**.

## Factory Function 
```js
  function createCircle1(radius) {
    return {
      radius: radius, // can just say 'radius' instead of 'radius: radius' if it's named the same 
      draw: function() {
        console.log('draw');
      }
    };

  }
```
Create a new object by assigning the called function to a variable:
```js
  const circle1 = createCircle1(2);
```
## Constructor Function
When defining a constructor function, the name of the constructor should be capitalized.

In the body of a constructor function, instead of returning an object like a factory function, you will use the *this* syntax. 
```js
  function CreateCircle2(radius) {
    this.radius = radius;
    this.draw = function() {
      console.log('draw');
    }
  }
```
Use the 'new' keyword to create a new object from the constructor:
```js
  const circle2 = new CreateCircle2(3);
```
'this' is a keyword that is a reference to the object being created by the above. 

Typing 'this' in the java console will return the window object.

Three things happen when creating a new object from a constructor function (using the 'new' operator):
1. A empty object is created.
1. It will set 'this' to point to the object.
1. It will return the new object.

### constructor property
Every object has a constructor property, which will reference the function that was used to create the object.
```js
  circle1.constructor
  circle2.constructor
```
'circle1' was created with a factory function, and will show that it was created from a built-in constructor function from Object. 'circle2' was created from a constructor function, it will show CreateCircle2 as its creator.

In other words, object literals and objects created from factory function are constructed from Object; objects created from constructor functions are constructed from that constructor function.

### *function*.call and *function*.apply
If you use the 'call' method of a constructor function, it will create a new object. The following two statements do the same thing:
```js
  const circle = CreateCircle2.call({},1);
  const circle = new CreateCircle2(1);
```
In the first expression, the first argument sets the context for 'this', so in the first statement above, the empty object is the context for 'this'.

Using the 'apply' method will let you pass an array as the second argument instead of a value.

## Value Types vs Reference Types
Value Types (or primitives):
* Number
* String
* Boolean
* Symbol
* undefined
* null

Reference Types (or objects):
* Object
* Function
* Array

Functions and arrays are also objects. Value types are primitives. So, in JavaScript, you have primitives and objects. This is important in understading mutability.

When using primitives:
```js
  let x = 10;
  let y = x;

  x = 20;
  // 'y' still equals 10
```
'y' will continue to equal 10 after this code runs, because y is independent from x. It makes a copy of the variable assigned to x. This is the way primitives behave.

When using objects:
```js
  let x = {val: 10};
  let y = x;

  x.val = 20;
  // 'y.val' equals 20
```
'y.val' will equal 20. y and x are references to the object, and are dependent.

In other words, when you assign x to y, and x is a primitive, then it assigns the value of x to y. 

On the other hand, when you assign x to y, and x is an object, it assigns the memory location of the object to y, instead of the value.

Similarly, if you pass a primitive as an argument to a function, it will pass a copy of the value. If you pass an object, it will pass a reference to the function.
```js
  let number = 10;

  function increase(number) {
    number++;
  }

  increase(number);
  console.log(number);
  // This will display 10
```
The console log will display 10, because the value of number was passed to the function, and because of local scope, that number variable is only visible to the function.

```js
  let object = { value:10 };

  function increase(object) {
    object.value++;
  }

  increase(object);
  console.log(object.value);
  // This will display 11
```
The console log will display 11, because a reference to the object is passed to the function.

**Primitives are copied by their value. Objects are copied by their reference.**

## Adding members to an object
Unlike other languages with real classes, you can add a new member (property or method) to a JavaScript object without first defining it in its class:
```js
  circle2.newMember = 'Value';
  circle2['newMember'] = 'Value';
```
## Deleting members from an object
```js
  delete circle.member
```
## Referencing members of an object
Use a variable to reference a member of an object:
```js
  let workingMember = "name";
  circle2[workingMember] = "Bill";
```
The above code will create or reassign circle2.name to "Bill".
## Enumerating members of an object
Enumerate keys:
```js
  for (let key in circle2) {
    console.log(key)
  }
```
Enumerate keys and their values. Use the bracket notation to access the values assigned to keys:
```js
  for (let key in circle2) {
    console.log(key, circle2[key]);
  }
```
Another method is using Object.keys:
```js
  const keys = Object.keys(circle2);
```
Object.keys will return an array of the circle2's members.

To see if an object has a member, use the 'in' keyword with an if statement:
```js
  if ('radius' in circle2) {
    console.log("circle2 object has a member named 'radius'.");
  }
```
## Using Abstraction to hide properties and methods
When writing a constructor function to create objects, you want to hide as many properties and methods as possible to minimize the impact of changes from other areas of code. Only expose the members necessary to work with the object. 

To hide, simply replace 'this' with let or a const expression:
```js
  function CreateCircle3(radius) {
    this.radius = radius;
    let defaultLocation = { x: 0, y: 0 };
    let computeOptimumLocation = function(factor) {
      //....
    }
    this.draw = function() {
      computeOptimumLocation(0.1);
      console.log('draw');
    };
  }
```
Ignore the pseudocode above, and just remember that defaultLocation and computerOptimumLocation will not be available/exposed as a property or method of any object created from the constructor function above.

Strictly speaking, defaultLocation and computeOptimumLocation are not 'hidden' members, there is no such thing in JavaScript; they are just locally-scoped variables in the CreateCircle3 function. But they act like hidden members.
## Getters and Setters
Use getters to read a 'hidden' value of an object:
```js
  function CreateCircle3(radius) {
    this.radius = radius;
    
    let defaultLocation = { x: 0, y: 0 };

    Object.defineProperty(this, 'defaultLocation', {
      get: function() {
        return defaultLocation;
      }
    });

    let computeOptimumLocation = function(factor) {
      //....
    }
    
    this.draw = function() {
      computeOptimumLocation(0.1);
      console.log('draw');
    };
  }

  const circle3 = new CreateCircle3(4);
  circle3.defaultLocation;
```
*note: if defaultLocation was just a string or number, then you would not be able to change its value using something like 'circle3.defaultLocation = 6". But since I assigned it to an object, I was able to change the x value outside the object by using the syntax 'circle3.defaultlocations.x = 8', even though it's supposed to be hidden. I think this is because its an object and it's a reference.*

Use setters to assign a value. In the code below, the setter will check if the x and y keys exist before letting the assignment continue:

```js
  function CreateCircle4(radius) {
    this.radius = radius;
    
    let defaultLocation = { x: 0, y: 0 };

    Object.defineProperty(this, 'defaultLocation', {
      get: function() {
        return defaultLocation;
      }
      set: function(value) {
        if (!value.x || !value.y) {
          throw new Error('Invalid location, bub.');
        }
        defaultLocation = value;
    });

    let computeOptimumLocation = function(factor) {
      //....
    }
    
    this.draw = function() {
      computeOptimumLocation(0.1);
      console.log('draw');
    };
  }

  const circle4 = new CreateCircle4(32);
  circle4.defaultLocation = {x:4,y:2};
```
