// Object literal
console.log("Creating an object called circle from an object literal:");
const circle = {
  radius: 1,
  location: {
    x: 1,
    y: 1
  },
  draw: function() {
    console.log('draw');
  }
};
console.log(circle);

// Factory Function
console.log("\nCreating an object called circle1 from a Factory Function:");
function createCircle1(radius) {
  return {
    radius,
    draw: function() {
      console.log('draw');
    }
  };
}
// create the new object from the Factory Function:
const circle1 = createCircle1(2);
console.log(circle1);

// Constructor Function
console.log("\nCreating an object called circle2 from a Constructor Function:");
function CreateCircle2(radius) {
  this.radius = radius;
  this.draw = function() {
    console.log('draw');
  }
}
// create a new object from the constructor
const circle2 = new CreateCircle2(4);
console.log(circle2);

// Primitives copy values, objects pass references
console.log("\nPrimitives copy values, objects pass references:");
let number = 10;

function increaseNum(number) {
  number++;
}

increaseNum(number);
console.log("The value of number stays the same: " + number);

let object = {value:10};

function increaseObj(object) {
  object.value++;
}

increaseObj(object);
console.log(`Whereas object.value increases: ${object.value}`);

// referencing an object member via variable
console.log("\nReferencing an object member via a variable:")
let workingMember = "name";
circle2[workingMember] = "Bill";
console.log("circle2.name equals " + circle2.name);
circle2[workingMember] = "Joe";
console.log("circle2.name was reassigned to " + circle2.name);

// enumerating members of an object
console.log("\nEnumerating members of an object");
for (let key in circle2) {
  console.log(key, circle2[key]);
}
console.log("\nEnumerating only the properties, not the methods, of an object:");
for (let key in circle2) {
  if (typeof(circle2[key]) !== 'function') {
    console.log(key, circle2[key]);
  }
}
console.log("\nDetermining if an object contains a member:");
if ('radius' in circle2) {
  console.log(`The object circle2 contains the member "radius".`);
} else {
  console.log(`The object circle2 does not contain the member "radius".`);
}
if ('somethingElse' in circle2) {
  console.log(`The object circle2 contains the member "somethingElse".`);
} else {
  console.log(`The object circle2 does not contain the member "somethingElse".`);
}

// using a getter to get the value of a hidden property of an object:
console.log("\nUsing a getter to get the value of a hidden property of the 'circle3' object:");

function CreateCircle3(radius) {
  this.radius = radius;
  let defaultLocation = 5; 
  Object.defineProperty(this,'defaultLocation',{
    get: function() {
      return defaultLocation;
   }
  });
  this.draw = function() {
    console.log('draw');
  };
}
const circle3 = new CreateCircle3(8);
console.log(circle3);

// using a setter to set the value of a hidden property of an object:
console.log("\nUsing a setter to set the value of a hidden property of the 'circle4' object:");

function CreateCircle4(radius) {
  this.radius = radius;
  let defaultLocation = 5; 
  Object.defineProperty(this,'defaultLocation',{
    get: function() {
      return defaultLocation;
    },
    set: function(value) {
      if (!value.x || !value.y) {
        throw new Error('Invalid location, bub.');
      }
      defaultLocation = value;
    }
  });
  this.draw = function() {
    console.log('draw');
  };
}
const circle4 = new CreateCircle4(8);
console.log(circle4);


// Stopwatch projet
function Stopwatch() {
  let startTime = 0, stopTime = 0;
  let duration = 0;
  let onOff = 0;

  this.reset = function() {
    duration = 0;
    onOff = 0;
  };

  this.start = function() {
    if (onOff===1) throw new Error('Stopwatch already started.');
    startTime = Date.now();
    onOff = 1;
  };

  this.stop = function() {
    if (onOff===0) throw new Error('Stopwatch is already stopped.');
    stopTime = Date.now();
    duration = stopTime-startTime+duration;
    onOff = 0;
  };

  Object.defineProperty(this,'duration',{
    get: function() {
      console.log(`Time elapsed: ${duration/1000} seconds.`);
      return duration;
    }
  });
}

const sw =  new Stopwatch();
