# Markdown 

Paragraphs are separated by a blank line.

This is a word in *italics*.

This is a word in **bold**.

This is a word in ***bold italics***.

This is a word in `monospace`.

Escape any character by using a backslash.

# Level One heading
## Level Two heading
### Level Three heading
#### Level Four heading

This is a link to a [remote site](https://duckduckgo.com). You can use this for a relative/local link too.

Autolink URLs, URLs "in the raw": <https://duckduckgo.com>. Others: <mailto:president@whitehouse.gov>, <tel:555-555-5555>.

Images: ![picture](markdown.png)

Image as a link: [![picture](markdown.png)](https://duckduckgo.com).

Unordered Lists
* One
* Two
* Three

Numbered Lists
1. One
1. Two
1. Three

Use 1 every time so you can reorder with having to change the numbers.  It will render your numbers automatically.

Nested Lists: Indent four spaces:
1. One
1. Two
    1. Two.one
    1. Two.two
    1. Two.three
1. Three
1. Four

Four dashes to put in a horizontal rule:

----

Hard returns are a way of starting a new line within a given paragraph.  Two spaces followed by the line return:

Roses are red  
Violets are blue

Blockquotes:
> Here is a block
>
>> Nested block
>

Code blocks separate text or code from the document, usually as a box.  Two types: plain (preformatted, as-is) and code fences (will format the text like a IDE).  Both use backticks to 'fence off' the text or code.

Preformatted:
```
    Roses are red
    Violets are blue
```
Code fence:
```js
  console.log("Hello World")
```
Supported code fence tags: md, json, js, html, css, sh (Markdown, JSON, JavaScript, HTML, CSS, Shell or Bash).

Tables are not supported in official Markdown language. Pandoc let's
you create tables.
